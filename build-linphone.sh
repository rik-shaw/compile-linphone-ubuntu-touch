#!/bin/bash

set -x
set -e

export CXX=arm-linux-gnueabihf-g++
export PKG_CONFIG_LIBDIR=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig:`pwd`../output/lib/pkgconfig:`pwd`../output/lib/arm-linux-gnueabihf/pkgconfig
export CC=arm-linux-gnueabihf-gcc
export CFLAGS="-march=armv7-a -mfpu=neon-vfpv4 -I../output/include"
export LDFLAGS="-L../output/lib"
export CXXFLAGS="-march=armv7-a -mfpu=neon-vfpv4 -I../output/include"
cmake . -DCMAKE_INSTALL_PREFIX=`pwd`/../output -DENABLE_GTK_UI=NO -DENABLE_DOC=NO -DENABLE_UNIT_TESTS=NO -DENABLE_NLS=NO -DENABLE_VIDEO=NO -DENABLE_X11=NO -DENABLE_STRICT=NO -DENABLE_VCARD=NO -DENABLE_ASSISTANT=NO -DENABLE_TOOLS=NO -DENABLE_SQLITE_STORAGE=NO -DENABLE_RELATIVE_PREFIX=YES -DENABLE_CXX_WRAPPER=NO -DENABLE_CSHARP_WRAPPER=NO
make install
