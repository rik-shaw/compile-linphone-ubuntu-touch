#!/bin/bash

OUTPUT=`pwd`/../output
PWD=`pwd`
USERID=`id -u`

set -x
set -e

docker run \
    -v $PWD/../:$PWD/../ \
    -w $PWD \
    -e CXX=arm-linux-gnueabihf-g++ \
    -e PKG_CONFIG_LIBDIR=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig:$OUTPUT/lib/pkgconfig:$OUTPUT/lib/arm-linux-gnueabihf/pkgconfig \
    -e CC=arm-linux-gnueabihf-gcc \
    -u $USERID \
    --rm -i bhdouglass/linphone-compile:xenial "$@"
